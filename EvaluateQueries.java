import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.Map.Entry;
  
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
// import lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;
import org.tartarus.snowball.ext.PorterStemmer;
    
public class EvaluateQueries {
      
    public static void main(String[] args) {
        //List of directories and important file names
        String cacmDocsDir = "data/cacm"; // directory containing CACM documents
        String medDocsDir = "data/med"; // directory containing MED documents
    
        String cacmIndexDir = "data/index/cacm"; // the directory where index is written into
        String medIndexDir = "data/index/med"; // the directory where index is written into
    
        String cacmQueryFile = "data/cacm_processed.query"; // CACM query file
        String cacmAnswerFile = "data/cacm_processed.rel"; // CACM relevance judgements file
        String medQueryFile = "data/med_processed.query"; // MED query file
        String medAnswerFile = "data/med_processed.rel"; // MED relevance judgements file
    
        String stopwordsFile = "data/stopwords/stopwords_indri.txt"; // stopwords file
    
        int cacmNumResults = 100;
        int medNumResults = 100;
          
        /*************************************************************************/
        // This blocks sets up the stopwords to be used for indexing
        CharArraySet stopwords = new CharArraySet(0, false);
        BufferedReader in = null;
        String line;
        try {
            in = new BufferedReader(new FileReader(new File(stopwordsFile)));
            while ((line = in.readLine()) != null) {
                // add stopword
                stopwords.add(line);
            }
        } catch (Exception e) {
            System.out.println(" caught a " + e.getClass()
                    + "\n with message: " + e.getMessage());
        }
        /*************************************************************************/   
        // Code to write index of CACM and MED collections to CSV file
        //This is no longer needed because the collections have been indexed and written to csv files
        // HashMap<String, HashMap<String, Integer>> index =
        // IndexFiles.buildIndex(cacmIndexDir, cacmDocsDir, stopwords);
        // HashMap<String, HashMap<String, Integer>> index2 =
        // IndexFiles.buildIndex(medIndexDir, medDocsDir, stopwords);
        // writeIndex("CACMindex.csv", index);
        // writeIndex("MEDindex.csv", index2);
        /*************************************************************************/
        //Parses through each query, removing stopwords
        Analyzer analyzer = new MyAnalyzer(stopwords);
        Map<Integer, String> queries = stopwordQueryMap(analyzer, cacmQueryFile);
        Map<Integer, String> queries_med = stopwordQueryMap(analyzer, medQueryFile);
  
        /*************************************************************************/
        //Question 1
         //Evaluates the MAP scores for the default Lucene similarity 
        //This, however, requires the original IndexFiles.java code, since we modified that file for Q3-4
        //This has been provided by methods with the same name except that end with _1
          
         //System.out.println(evaluate(cacmIndexDir, cacmDocsDir,
         //cacmQueryFile, cacmAnswerFile, cacmNumResults, stopwords));
         //System.out.println(" MAP CACM"); System.out.println("\n");
           
           
         //System.out.println(evaluate(medIndexDir, medDocsDir, medQueryFile,
         //medAnswerFile, medNumResults, stopwords));
         //System.out.println(" MAP MED");
           
          
        /*************************************************************************/
        //This code block builds and writes the indexes for CACM and MED
        //Commented out since only needed to run once, since indexes have already been written to files
          
        // HashMap<String, HashMap<String, Integer>> doc_index =
        // IndexFiles.buildIndex(cacmIndexDir, cacmDocsDir, stopwords);
           
        // HashMap<String, HashMap<String, Integer>> doc_index_med =
        // IndexFiles.buildIndex(medIndexDir, medDocsDir, stopwords);
          
        //This code adds the Document Frequency measures to each term of each document in the indexes
        // HashMap<String, HashMap<String, Integer[]>> doc_indexIDF =
        // IndexFiles.buildIdfIndex(doc_index);
        // writeIdfIndex("CACMindex_df.csv", doc_indexIDF);
          
        // HashMap<String, HashMap<String, Integer[]>> doc_indexIDF_med =
        // IndexFiles.buildIdfIndex(doc_index_med);
        // writeIdfIndex("MEDindex_df.csv", doc_indexIDF_med);
        /*************************************************************************/

      //Parses the already written csv file containing the document index into usable form
        HashMap<String, HashMap<String, Integer[]>> doc_index = csv_to_HashMap(
                "CACMindex_df.csv", cacmDocsDir, ";"); //semicolon is the separator
        //stems each term and then indexes all the queries for CACM
        HashMap<Integer, HashMap<String, Integer[]>> query_index = buildQueryMap_idf(
                queries, analyzer, doc_index); //analyzer initialized earlier
          
        HashMap<String, HashMap<String, Integer[]>> doc_index_med = csv_to_HashMap(
                "MEDindex_df.csv", medDocsDir, ";");
        //does the same for the MED collection
        HashMap<Integer, HashMap<String, Integer[]>> query_index_med = buildQueryMap_idf(
                queries_med, analyzer, doc_index_med);
    

        /*************************************************************************/
          
        //This is just some test code to make sure writing to CSV and parsing it back
        //are completely error free and exactly reproduced
          
        // writeIdfIndex("CACMindex_df.csv", csv_to_HashMap("CACMindex_df.csv",
        // cacmDocsDir, ";"));
        // csv_to_HashMap("CACMindex_df.csv", cacmDocsDir, ";");
  
         
        /*******************************PART 1 CACM******************************************/
        /*
        writeIdfIndex_query("query_index.txt", query_index);
         System.out.println("MAP CACM atc_atc: " + atc_atc(query_index, doc_index, cacmAnswerFile, "CACM_atc_atc_results.txt","rocchio_index.txt"));
          
          
         HashMap<Integer, HashMap<String, HashMap<String, Double>>> roc_query_indexed_docs = read_rocchio_index ("rocchio_index.txt", cacmDocsDir, ";");
          
        HashMap<Integer, ArrayList<String>> query_indexed_rel_docs = processed_to_HashMap("data/cacm_processed.rel");
        //System.out.println(query_indexed_rel_docs);
         
        HashMap<Integer, ArrayList<String>> list_words = unique_terms(roc_query_indexed_docs, doc_index, query_index);
        //System.out.println(list_words);
        HashMap<Integer, HashMap<String, Double>> query_w_weight =  query_weights(query_index,doc_index);
          
        HashMap<Integer, HashMap<String, Double>> new_query_index = rocchio(query_w_weight, 
            roc_query_indexed_docs, 4, 8, 0, 5, list_words, query_indexed_rel_docs, query_index,doc_index  );
         
        writeIndex_query("new_query_index.txt", new_query_index);
          
        query_index = buildQueryMap_idf(
                queries, analyzer, doc_index);
        System.out.println("MAP CACM atc_atc_new: " + atc_atc_new(new_query_index, doc_index, cacmAnswerFile, "CACM_atc_atc_results.txt")); 
        */
         
        /*******************************PART 1 MED******************************************/     
        /*
        writeIdfIndex_query("query_index_med.txt", query_index_med);
          
        System.out.println("MAP MED atc_atc: " + atc_atc(query_index_med, 
                doc_index_med, medAnswerFile, "MED_atc_atc_results.txt","rocchio_index_med.txt"));
        HashMap<Integer, HashMap<String, HashMap<String, Double>>> roc_query_indexed_docs_med = 
                read_rocchio_index ("rocchio_index_med.txt", medDocsDir, ";");
        HashMap<Integer, ArrayList<String>> query_indexed_rel_docs_med = 
                processed_to_HashMap("data/med_processed.rel");
        HashMap<Integer, ArrayList<String>> list_words_med = 
                unique_terms(roc_query_indexed_docs_med, doc_index_med, query_index_med);
        HashMap<Integer, HashMap<String, Double>> query_w_weight_med =  
                query_weights(query_index_med,doc_index_med);
        HashMap<Integer, HashMap<String, Double>> new_query_index_med = 
                rocchio(query_w_weight_med, roc_query_indexed_docs_med, 4, 8, 0, 5, list_words_med, 
                        query_indexed_rel_docs_med, query_index_med, doc_index_med  );
        writeIndex_query("new_query_index_med.txt", new_query_index_med);
        query_index_med = buildQueryMap_idf(queries_med, analyzer, doc_index_med);
         
        System.out.println("MAP MED atc_atc_new: " + atc_atc_new(new_query_index_med, 
                doc_index_med, medAnswerFile, "MED_atc_atc_results.txt")); 
         */
        
        /*******************************PART 2 CACM******************************************/  
        /*
        LinkedHashMap<Integer, LinkedHashMap<String, Double>> clustered_30 = complete_clustering(
                atc_atc_clustering(query_index, doc_index, cacmAnswerFile, 
                "CACM_atc_atc_results.txt"), 10, 30, doc_index, query_index);
        LinkedHashMap<Integer, LinkedHashMap<String, Double>> atc_100 = atc_atc_100(query_index, doc_index, 
                cacmAnswerFile, "CACM_atc_atc_results.txt" ); 
        System.out.println("MAP CACM atc_atc_clustered: " + recalc_MAP_clustering(clustered_30, atc_100, cacmAnswerFile));
         */
         
        /*******************************PART 2 MED******************************************/  
        /*
        LinkedHashMap<Integer, LinkedHashMap<String, Double>> clustered_30_med = complete_clustering(
                atc_atc_clustering(query_index_med, doc_index_med, medAnswerFile, 
                "MED_atc_atc_results.txt"), 10, 30, doc_index_med, query_index_med);
        LinkedHashMap<Integer, LinkedHashMap<String, Double>> atc_100_med = atc_atc_100(query_index_med, doc_index_med, 
                medAnswerFile, "MED_atc_atc_results.txt" ); 
        System.out.println("MAP MED atc_atc_clustered: " + recalc_MAP_clustering(clustered_30_med, atc_100_med, medAnswerFile));
         */
        
        /*******************************PART 3 CACM******************************************/
        /*
        writeIdfIndex_query("query_index.txt", query_index);
         System.out.println("MAP CACM atc_atc: " + atc_atc(query_index, doc_index, cacmAnswerFile, "CACM_atc_atc_results.txt","rocchio_index.txt"));
          
          
         HashMap<Integer, HashMap<String, HashMap<String, Double>>> roc_query_indexed_docs = read_rocchio_index ("rocchio_index.txt", cacmDocsDir, ";");
          
        HashMap<Integer, ArrayList<String>> query_indexed_rel_docs = processed_to_HashMap("data/cacm_processed.rel");
        //System.out.println(query_indexed_rel_docs);
         
        HashMap<Integer, ArrayList<String>> list_words = unique_terms(roc_query_indexed_docs, doc_index, query_index);
        //System.out.println(list_words);
        HashMap<Integer, HashMap<String, Double>> query_w_weight =  query_weights(query_index,doc_index);
          
        HashMap<Integer, HashMap<String, Double>> new_query_index = rocchio_part3(query_w_weight, 
            roc_query_indexed_docs, 4, 16, 0, 5, list_words, query_indexed_rel_docs, query_index,doc_index  );
         
        writeIndex_query("new_query_index.txt", new_query_index);
          
        query_index = buildQueryMap_idf(
                queries, analyzer, doc_index);
        System.out.println("MAP CACM atc_atc_new: " + atc_atc_new(new_query_index, doc_index, cacmAnswerFile, "CACM_atc_atc_results.txt")); 
        */
          
          
        /*******************************PART 3 MED******************************************/     
        /*
        writeIdfIndex_query("query_index_med.txt", query_index_med);
          
        System.out.println("MAP MED atc_atc: " + atc_atc(query_index_med, 
                doc_index_med, medAnswerFile, "MED_atc_atc_results.txt","rocchio_index_med.txt"));
        HashMap<Integer, HashMap<String, HashMap<String, Double>>> roc_query_indexed_docs_med = 
                read_rocchio_index ("rocchio_index_med.txt", medDocsDir, ";");
        HashMap<Integer, ArrayList<String>> query_indexed_rel_docs_med = 
                processed_to_HashMap("data/med_processed.rel");
        HashMap<Integer, ArrayList<String>> list_words_med = 
                unique_terms(roc_query_indexed_docs_med, doc_index_med, query_index_med);
        HashMap<Integer, HashMap<String, Double>> query_w_weight_med =  
                query_weights(query_index_med,doc_index_med);
        HashMap<Integer, HashMap<String, Double>> new_query_index_med = 
                rocchio_part3(query_w_weight_med, roc_query_indexed_docs_med, 4, 16, 0, 5, list_words_med, 
                        query_indexed_rel_docs_med, query_index_med, doc_index_med  );
        writeIndex_query("new_query_index_med.txt", new_query_index_med);
        query_index_med = buildQueryMap_idf(queries_med, analyzer, doc_index_med);
         
        System.out.println("MAP MED atc_atc_new: " + atc_atc_new(new_query_index_med, 
                doc_index_med, medAnswerFile, "MED_atc_atc_results.txt")); 
        */   

    }
     
    static LinkedHashMap<Integer, LinkedHashMap<String, Double>> complete_clustering(
            LinkedHashMap<Integer, LinkedHashMap<String, Double>> cluster_index, 
            int k, int num_docs, HashMap<String, HashMap<String, Integer[]>> doc_index, 
            HashMap<Integer, HashMap<String, Integer[]>> query_index){
         
        LinkedHashMap<Integer, LinkedHashMap<String, Double>> new_cluster_index =
                new LinkedHashMap<Integer, LinkedHashMap<String, Double>>();
         
         
        LinkedHashMap<String, LinkedHashMap<String, Double>> cluster_k = 
                new LinkedHashMap<String, LinkedHashMap<String, Double>>();
         
        int strLen = 0;
        for(String doc: cluster_index.get(1).keySet()){
            strLen = doc.length();
            break;
        }
         
         
        for (Integer i: cluster_index.keySet()){ //for each query
             
            LinkedHashMap<String, LinkedHashMap<String, Double>> distance_grid = 
                    new LinkedHashMap<String, LinkedHashMap<String, Double>>();
             
            String[] closest_docs = {"",""};
 
            double current_min_dis =0;
             
            for (String d1: cluster_index.get(i).keySet()){ //for each document
                LinkedHashMap<String, Double> distances = new LinkedHashMap<String, Double>();
                for (String d2: cluster_index.get(i).keySet()){//for each document
                    double current_cluster_distance = cluster_distance(atc_atc_doc_weights(query_index, doc_index, i, d1), 
                            atc_atc_doc_weights(query_index, doc_index, i, d2));
                    distances.put(d2, current_cluster_distance);
                     
                    if(!d1.equals(d2) && (closest_docs[0].equals("") || closest_docs[1].equals(""))){
                        closest_docs[0] =d1;
                        closest_docs[1] =d2;
                        current_min_dis = current_cluster_distance;
                    }
                    if(!d1.equals(d2) && current_cluster_distance < current_min_dis){
                        closest_docs[0] =d1;
                        closest_docs[1] =d2;
                        current_min_dis = current_cluster_distance;
                    }
 
                     
                }
                distance_grid.put(d1, distances);
                 
            }
            cluster_k = distance_grid;
            for (int x=0; x<(num_docs - k); x++){
                cluster_k = cluster_next(cluster_k, current_min_dis, closest_docs);
                 
                double current_cluster_distance = 0;
                current_min_dis =0;
                closest_docs[0] = "";
                closest_docs[1] = "";
                 
                for (String d1: cluster_k.keySet()){ //for each document
 
                    for (String d2: cluster_k.get(d1).keySet()){//for each document
                        current_cluster_distance = cluster_k.get(d1).get(d2);
                        if(!d1.equals(d2) && current_min_dis == 0){
                            closest_docs[0] =d1;
                            closest_docs[1] =d2;
                            current_min_dis = current_cluster_distance;
                            //System.out.println(closest_docs[0] +closest_docs[1]);
                        }
                        if(!d1.equals(d2) && current_cluster_distance < current_min_dis){
                            closest_docs[0] =d1;
                            closest_docs[1] =d2;
                            current_min_dis = current_cluster_distance;
                            //System.out.println(d1+" " + d2);
                        }
 
                         
                    }
                     
                }
                 
            }
             
            //now we have k clusters, need to rerank 30 docs
            //rerank cluster_index
             
            LinkedHashMap<String, Double> max_rank_cluster = new LinkedHashMap<String, Double>();
             
            for(String c: cluster_k.keySet()){
                if(c.length() == strLen){
                    max_rank_cluster.put(c, cluster_index.get(i).get(c));
                }else if(c.length() > strLen){
                    String cluster = c;
                    double largest = 0;
                    while(cluster.length() > 1){
                        String doc = cluster.substring(0, strLen);
                        if (cluster.length() > strLen){
                            cluster = cluster.substring(strLen);
                        }else{
                            cluster = "";
                        }
                        if(cluster_index.get(i).get(doc) > largest){
                            largest = cluster_index.get(i).get(doc);
                        }   
                    }
                    max_rank_cluster.put(c, largest);
                }
            }
             
            ValueComparator bvc =  new ValueComparator(max_rank_cluster);
            //http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
            TreeMap<String, Double> sorted_docs = new TreeMap<String, Double>(bvc);
            sorted_docs.putAll(max_rank_cluster); 
    
            LinkedHashMap<String, Double> sorted_clusters = new LinkedHashMap<String, Double>();
             
            for (Map.Entry<String,Double> entry : sorted_docs.entrySet()) {
                sorted_clusters.put(entry.getKey(), entry.getValue());
            }
             
            LinkedHashMap<String, Double> sorted_rerank= new LinkedHashMap<String, Double>();
             
            for (String s: sorted_clusters.keySet()){
                if(s.length() == strLen){
                    sorted_rerank.put(s, sorted_clusters.get(s));
                }else if(s.length() > strLen){
                    String cluster = s;
                    while(cluster.length() > 1){
                        String doc = cluster.substring(0, strLen-1);
                        if (cluster.length() > strLen){
                            cluster = cluster.substring(strLen);
                        }else{
                            cluster = "";
                        }
                        sorted_rerank.put(doc, sorted_clusters.get(s)); 
                    }
                }
            }
            new_cluster_index.put(i, sorted_rerank);    
        }
         
        return new_cluster_index;
    }
     
    static double recalc_MAP_clustering(LinkedHashMap<Integer, LinkedHashMap<String, Double>> clustered_30,
            LinkedHashMap<Integer, LinkedHashMap<String, Double>> atc_100, String answerFile){
         
        double MAP = 0;
        LinkedHashMap<Integer, LinkedHashMap<String, Double>> new_100 = 
                new LinkedHashMap<Integer, LinkedHashMap<String, Double>>();
         
        for(Integer i: atc_100.keySet()){
             
            LinkedHashMap<String, Double> new_100_query = new LinkedHashMap<String, Double>();
             
            for(String d: clustered_30.get(i).keySet()){
                new_100_query.put(d, clustered_30.get(i).get(d));
            }
            int count = 0;
            for(String d: atc_100.get(i).keySet()){
                if(count > 29){
                    new_100_query.put(d, atc_100.get(i).get(d));
                }
                count++;
            }
             
            double AP =precision_LinkedMap(loadAnswers(answerFile).get(i), new_100_query);
            MAP += AP;
            new_100.put(i, new_100_query);
             
        }
        //System.out.println(new_100);
        MAP /= clustered_30.size();
         
        return MAP;
    }
     
    static LinkedHashMap<String, LinkedHashMap<String, Double>> cluster_next(
            LinkedHashMap<String, LinkedHashMap<String, Double>> grid, double min_dist, String[] closest_docs){
         
        /*
         * 
         */
        LinkedHashMap<String, LinkedHashMap<String, Double>> new_grid =
                new LinkedHashMap<String, LinkedHashMap<String, Double>>();
        String new_cluster = closest_docs[0] + closest_docs[1];
         
        for (String dA: grid.keySet()){
             
            LinkedHashMap<String, Double> new_dist = new LinkedHashMap<String, Double>();
             
            for (String dB: grid.get(dA).keySet()){
                if(dA.equals(dB)){
                    if(!dA.equals(closest_docs[0]) && !dA.equals(closest_docs[1])
                            && !dB.equals(closest_docs[0]) && !dB.equals(closest_docs[1])){
                        new_dist.put(dA, 1.0);
                    }else if(dA.equals(closest_docs[0])){
                        new_dist.put(new_cluster, 1.0);
                    }
                     
                }else{
                    if(!dA.equals(closest_docs[0]) && !dA.equals(closest_docs[1])
                            && !dB.equals(closest_docs[0]) && !dB.equals(closest_docs[1])){
                        new_dist.put(dB, grid.get(dA).get(dB));
                    }else if(dA.equals(closest_docs[0])){
                        if(!dB.equals(closest_docs[0]) && !dB.equals(closest_docs[1])){
                            new_dist.put(dB, Math.max(grid.get(dA).get(dB), grid.get(closest_docs[1]).get(dB)));
                        }else if(dB.equals(closest_docs[1])){
                            //do nothing
                        }
                         
                    }else if(dA.equals(closest_docs[1])){
                        //do nothing
                    }else if(!dA.equals(closest_docs[0]) && !dA.equals(closest_docs[1])){
                        if(dB.equals(closest_docs[0])){
                            //System.out.println(dA + " " + closest_docs[1] + " " + grid);
                            new_dist.put(new_cluster, Math.max(grid.get(dA).get(closest_docs[0]), 
                                    grid.get(dA).get(closest_docs[1])));
                        }else if(dB.equals(closest_docs[1])){
                            //do nothing
                        }
                    }
                }
            }
            if(dA.equals(closest_docs[0])){
                new_grid.put(new_cluster, new_dist);
            }else if(!dA.equals(closest_docs[0]) && !dA.equals(closest_docs[1])){
                new_grid.put(dA, new_dist);
            }
        }
         
         
        return new_grid;
    }
     
     
    static double cluster_distance(HashMap<String, Double> c1, HashMap<String, Double> c2){
        double dotProduct = 0;
        for (String term: c1.keySet()){//for each term in c1
            if (c2.containsKey(term)){ //for each term in c2
                dotProduct += c1.get(term)*c2.get(term);
            }
             
        }
        if (dotProduct>0)
            return 1/dotProduct;
        else
            return 99999;
    }
     
     
    static LinkedHashMap<Integer, LinkedHashMap<String, Double>> atc_atc_100(
            HashMap<Integer, HashMap<String, Integer[]>> query_index,
            HashMap<String, HashMap<String, Integer[]>> doc_index, String answerFile, String printFile) {
         
        double MAP = 0;
 
        LinkedHashMap<Integer, LinkedHashMap<String, Double>> top100_results = 
                new LinkedHashMap<Integer, LinkedHashMap<String, Double>>();
         
        HashMap<Integer, HashMap<String, HashMap<String, Double>>> queries_docs_terms = 
                new HashMap<Integer, HashMap<String, HashMap<String, Double>>>();
          
        //PrintWriter writer;
        try {
            //writer = new PrintWriter(printFile, "UTF-8");
            
            for (Integer i : query_index.keySet()) { // queries
                //if(i==13){
                HashMap<String, Double> docScores = new HashMap<String, Double>();
                  
                HashMap<String, HashMap<String, Double>> docs_terms = new HashMap<String, HashMap<String, Double>>();
                  
                  
                for (String j : doc_index.keySet()) { // docs
                    HashMap<String, Double> weights = atc_atc_weights(query_index, doc_index, i, j);
                    double increment_weight = 0;
                    for (String weight: weights.keySet()){
                        increment_weight += weights.get(weight);
                    }
                    docScores.put(j, increment_weight);
                      
                    // System.out.println("Query:"+i+" "+docScores);
                }
                //if(i==13){
                    //System.out.println("Query:"+i+" "+docScores);
                //}
                // sort
                ValueComparator bvc =  new ValueComparator(docScores);
                //http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
                TreeMap<String, Double> sorted_docs = new TreeMap<String, Double>(bvc);
  
                  
                sorted_docs.putAll(docScores); 
                 
        
                LinkedHashMap<String, Double> sorted_100 = new LinkedHashMap<String, Double>();
                // sorted_100.put("", sorted_docs.get(y));
                // add some entries
                int count =0;
                for (Map.Entry<String,Double> entry : sorted_docs.entrySet()) {
                    sorted_100.put(entry.getKey(), entry.getValue());
                    count++;
                    if(count>99){
                        break;
                    }
                }
                    
                double AP =precision_LinkedMap(loadAnswers(answerFile).get(i), sorted_100);
                MAP += AP;
                top100_results.put(i, sorted_100);
                 
                  
            }//}
            MAP /= query_index.size();
              
        } catch (Exception e) { 
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
          
        return top100_results;
    }
     
    static LinkedHashMap<Integer, LinkedHashMap<String, Double>> atc_atc_clustering(
            HashMap<Integer, HashMap<String, Integer[]>> query_index,
            HashMap<String, HashMap<String, Integer[]>> doc_index, String answerFile, String printFile) {
         
        double MAP = 0;
 
        //Date start = new Date();
       // Date end= new Date();
        //System.out.println(end.getTime() - start.getTime() + " total milliseconds");
          
          
      //Hashmap of query number, hashmap of doc name, hashmap of terms in doc -> value = cosine weight
        //HashMap<Integer, HashMap<String, HashMap<String, Double>>>;
        LinkedHashMap<Integer, LinkedHashMap<String, Double>> top30_results = 
                new LinkedHashMap<Integer, LinkedHashMap<String, Double>>();
         
        HashMap<Integer, HashMap<String, HashMap<String, Double>>> queries_docs_terms = 
                new HashMap<Integer, HashMap<String, HashMap<String, Double>>>();
          
        //PrintWriter writer;
        try {
            //writer = new PrintWriter(printFile, "UTF-8");
            
            for (Integer i : query_index.keySet()) { // queries
                //if(i==13){
                HashMap<String, Double> docScores = new HashMap<String, Double>();
                  
                HashMap<String, HashMap<String, Double>> docs_terms = new HashMap<String, HashMap<String, Double>>();
                  
                  
                for (String j : doc_index.keySet()) { // docs
                    HashMap<String, Double> weights = atc_atc_weights(query_index, doc_index, i, j);
                    double increment_weight = 0;
                    for (String weight: weights.keySet()){
                        increment_weight += weights.get(weight);
                    }
                    docScores.put(j, increment_weight);
                      
                    // System.out.println("Query:"+i+" "+docScores);
                }
                //if(i==13){
                    //System.out.println("Query:"+i+" "+docScores);
                //}
                // sort
                ValueComparator bvc =  new ValueComparator(docScores);
                //http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
                TreeMap<String, Double> sorted_docs = new TreeMap<String, Double>(bvc);
  
                  
                sorted_docs.putAll(docScores); 
                 
        
                LinkedHashMap<String, Double> sorted_100 = new LinkedHashMap<String, Double>();
                LinkedHashMap<String, Double> sorted_30 = new LinkedHashMap<String, Double>();
                // sorted_100.put("", sorted_docs.get(y));
                // add some entries
                int count = 0;
                for (Map.Entry<String,Double> entry : sorted_docs.entrySet()) {
                    sorted_100.put(entry.getKey(), entry.getValue());
                    if (count < 30){
                        sorted_30.put(entry.getKey(), entry.getValue());
                    }
                    count++;
                    if (count > 99){
                        break;
                    }
                }
                    
                double AP =precision_LinkedMap(loadAnswers(answerFile).get(i), sorted_100);
                MAP += AP;
                //System.out.println("atc.atc Query: " + i + " AvgPrecision: " + AP + " " +  sorted_7);
                //writer.println("atc.atc Query: " + i + " AvgPrecision: " + AP + " " +  sorted_30);
                top30_results.put(i, sorted_30);
                 
                for (String rel_doc: sorted_30.keySet()){
                    docs_terms.put(rel_doc, atc_atc_doc_weights(query_index, doc_index, i, rel_doc));
                }
                queries_docs_terms.put(i, docs_terms);
                  
            }//}
            MAP /= query_index.size();
              
            //writer = new PrintWriter("rocchio_index.txt", "UTF-8");
            //writer.println(queries_docs_terms);
            //writer.close();
             
        } catch (Exception e) { 
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
       // MAP /= query_index.size();
       // writer.println("atc.atc Collection MAP: " + MAP);
        //writer.close();
        //System.out.println(queries_docs_terms);
        //write_rocchio_index("rocchio_index_med.txt", queries_docs_terms);
          
        return top30_results;
    }
   
    static HashMap<Integer, HashMap<String, Double>> rocchio(HashMap<Integer, HashMap<String, Double>> query, 
            HashMap<Integer, HashMap<String, HashMap<String, Double>>> query_indexed_docs, Integer A, Integer B, 
            Integer C, Integer K, HashMap<Integer, ArrayList<String>> list_words, HashMap<Integer, ArrayList<String>> query_indexed_rel_docs, 
            HashMap<Integer, HashMap<String, Integer[]>> query_index_atc_atc, HashMap<String, HashMap<String, Integer[]>> doc_index){
        //query: query #: (query term, term weight)
        //query_indexed_docs: query #: (Document(doc term, term weight) )
        //query_indexed_rel_docs: query #: (rel doc)
        HashMap<String, Double> rocchio_term_Scores = new HashMap<String, Double>();
          
         HashMap<Integer, HashMap<String, Double>> q_final = new  HashMap<Integer, HashMap<String, Double>>();
          
        double rocchio_score =0;
        double rocchio_partA =0;
        int rel_doc_count =0;
        int non_rel_doc_count =0;
          
        for (Integer query_number : query.keySet()){ //for each query
            double rocchio_partB =0;
            double rocchio_partC =0;
            HashMap<String, Double> roc_value = new HashMap<String, Double>();
            //q_final.put(query_number, null); //initalize new q_final query
           // System.out.println("inti: "+q_final.get(query_number));
            //System.out.println(query_number + " " + list_words);
            for(int i = 0; i < list_words.get(query_number).size(); i++){ //for each unique word
                  
                String current_unique_word= list_words.get(query_number).get(i);
                  
                if(query.get(query_number).containsKey(current_unique_word)){  //if word in query...works b/c key is term
                    double q_weight = query.get(query_number).get(current_unique_word);
                    rocchio_partA = A*q_weight;
                     
                }
                  
                for(String doc_id : query_indexed_docs.get(query_number).keySet()){ //goes through top 7 documents of a query
                      
                    if(query_indexed_docs.get(query_number).get(doc_id).containsKey(current_unique_word)){
                        //System.out.println(query_indexed_docs.get(query_number).get(doc_id));
                        double d_weight = query_indexed_docs.get(query_number).get(doc_id).get(current_unique_word);
                        rocchio_partB = rocchio_partB + d_weight;   
                       // if(query_number==1){System.out.println(current_unique_word);}
                          
                    }
                                  
          
                          
                      
                    /*
                     * for(int x=0; x<query_indexed_rel_docs.get(query_number).size(); x++){ //goes through each rel document of a query
                        //for(String rel_docs: query_indexed_rel_docs.get(query_number))
                        if(query_indexed_rel_docs.get(query_number).get(x).equals(doc_id)){ //if doc is relevant    
                            //query_indexed_rel_docs.get(rel_docs).equals(doc_id)
                            rel_doc_count = rel_doc_count + 1;
                            if(query_indexed_docs.get(query_number).get(doc_id).containsKey(current_unique_word)){ //if word in document
                                //if(query.get(doc_id).containsKey(list_words[i])){
                                double d_weight = query_indexed_docs.get(query_number).get(doc_id).get(current_unique_word);
                                rocchio_partB = rocchio_partB + d_weight;   
                            }
          
                        }
                        else{ //document is not relevant
                            non_rel_doc_count = non_rel_doc_count + 1;                      
                            if(query_indexed_docs.get(query_number).get(doc_id).containsKey(current_unique_word)){ //if word in document
                                double d_weight = query_indexed_docs.get(query_number).get(doc_id).get(current_unique_word);
                                rocchio_partC = rocchio_partC + d_weight;   
                            }
                        }
                    }
                     */
                }
                rel_doc_count =7;
                rocchio_partB = B*(1.0/rel_doc_count)*rocchio_partB;
                rocchio_partC=0;
                //System.out.println(current_unique_word + " " + rocchio_score);
                //rocchio_partC = C*(1/non_rel_doc_count)*rocchio_partC;
                rocchio_score = rocchio_partA + rocchio_partB - rocchio_partC;
                //if(query_number==1)System.out.println(current_unique_word + " " + rocchio_partB);
                //System.out.println(current_unique_word + " " + rocchio_score);
                  
                /*
                 * Compute Rocchio weights only for terms occurring in a relevant document or the query
                 *  (all other terms will have 0 or negative weights)
                 */
                if((rocchio_partA !=0) || (rocchio_partB !=0 )){ 
                    rocchio_term_Scores.put(current_unique_word, rocchio_score);
                }
                 
                rocchio_partA = 0;
                rocchio_partB = 0;
                  
            }
             
             // sort
            LinkedHashMap<String, Double> sorted_terms = new LinkedHashMap<String, Double>();
            sorted_terms = sortHashMapByValuesD(rocchio_term_Scores);
            // System.out.println("Query: "+ i);
            // System.out.println("Query: "+ i + sorted_docs);
            //if (query_number==1)System.out.println(sorted_terms);
            LinkedHashMap<String, Double> sorted_total = new LinkedHashMap<String, Double>();
            //LinkedHashMap<String, Double> sorted_25 = new LinkedHashMap<String, Double>();
            // sorted_100.put("", sorted_docs.get(y));
            // add some entries
            List<Entry<String, Double>> entryList = new ArrayList<Map.Entry<String, Double>>(
                    sorted_terms.entrySet());
           // int count = 0;
            for (int y = entryList.size() - 1; y > 0; y--) {
                if (y >= 0) {
                    Entry<String, Double> lastEntry = entryList.get(y);
                    sorted_total.put(lastEntry.getKey(), lastEntry.getValue());
                }
               // count++;
            }
             
             
             int count_expand =0;
             //System.out.println("here");
             //System.out.println(query_number + ": "+sorted_total);
           //query_index_atc_atc: query #: (query term, [tf, df])
            for(String sorted_rocchio_term : sorted_total.keySet()){//go through sorted terms
                //System.out.println(sorted_rocchio_term);
                //System.out.println(q_final);
                if(query.get(query_number).containsKey(sorted_rocchio_term)){
                     
                     roc_value.put(sorted_rocchio_term, sorted_total.get(sorted_rocchio_term));
                     
                   // q_final.put(sorted_rocchio_term, sorted_total.get(sorted_rocchio_term)); // insert new roc. weights for each term in query.                   
                   // System.out.println(q_final);
                }
                 
                if(count_expand < K){
                    int tf=0;
                     
                    //System.out.println("query: "+query_number);
                    //System.out.println("current term: "+sorted_rocchio_term);
                    if(!(query.get(query_number).containsKey(sorted_rocchio_term))){//if query does not contain term
                          
                        count_expand = count_expand + 1;
                         roc_value.put(sorted_rocchio_term, sorted_total.get(sorted_rocchio_term));
                        /*
                        System.out.println(count_expand + ": "+sorted_rocchio_term);
                        count_expand = count_expand + 1;
                        tf = 1;
                          
                        Integer[] x = new Integer[2];
                        x[0] = tf;
                        x[1] = 0;
                          
                        for(String doc_id : doc_index.keySet()){
                            if(doc_index.get(doc_id).containsKey(sorted_rocchio_term)){
                                x[1] = doc_index.get(doc_id).get(sorted_rocchio_term)[1];
                                break;
                            }
                        }
                          
                        //int df = query_index_atc_atc.get(query_number).get(sorted_rocchio_term);
                        query_index_atc_atc.get(query_number).put(sorted_rocchio_term, x);
                        //System.out.println(query_index_atc_atc.get(query_number).toString());
                          
                        */
                    }
                }
                  
            }
            
            q_final.put(query_number, roc_value);
             
             
              
        }
          
          
        //System.out.println(q_final);
        return q_final;
         
     
    }
 
    static HashMap<Integer, HashMap<String, Double>> rocchio_part3(HashMap<Integer, HashMap<String, Double>> query, 
            HashMap<Integer, HashMap<String, HashMap<String, Double>>> query_indexed_docs, Integer A, Integer B, 
            Integer C, Integer K, HashMap<Integer, ArrayList<String>> list_words, HashMap<Integer, ArrayList<String>> query_indexed_rel_docs, 
            HashMap<Integer, HashMap<String, Integer[]>> query_index_atc_atc, HashMap<String, HashMap<String, Integer[]>> doc_index){
        //query: query #: (query term, term weight)
        //query_indexed_docs: query #: (Document(doc term, term weight) )
        //query_indexed_rel_docs: query #: (rel doc)
         
        HashMap<String, Double> rocchio_term_Scores = new HashMap<String, Double>();
 
        HashMap<Integer, HashMap<String, Double>> q_final = new  HashMap<Integer, HashMap<String, Double>>();
          
        double rocchio_score =0;
        double rocchio_partA =0;
          
        for (Integer query_number : query.keySet()){ //for each query
            double rocchio_partB =0;
            double rocchio_partC =0;
            HashMap<String, Double> roc_value = new HashMap<String, Double>();
             
            HashMap<Integer, HashMap<String, HashMap<String, Double>>> query_indexed_docs_rel = 
                    new HashMap<Integer, HashMap<String, HashMap<String, Double>>>();
            HashMap<String, HashMap<String, Double>> new_doc_rel =
                    new HashMap<String, HashMap<String, Double>>();
             
            HashMap<Integer, HashMap<String, HashMap<String, Double>>> query_indexed_docs_nonrel = 
                    new HashMap<Integer, HashMap<String, HashMap<String, Double>>>();
            HashMap<String, HashMap<String, Double>> new_doc_nonrel =
                    new HashMap<String, HashMap<String, Double>>();
 
             
            int rel_doc_count =0;
            int non_rel_doc_count =0;
             
            for(String doc_id : query_indexed_docs.get(query_number).keySet()){ //goes through top 7 documents of a query
                  
                for(int x=0; x<query_indexed_rel_docs.get(query_number).size(); x++){ //goes through each rel document of a query
                      
                    if(rel_doc_count==0 || non_rel_doc_count==0 ){ //Go through loop if you have not found highest rel doc or highest no-rel doc
 
                        if(doc_id.contains(query_indexed_rel_docs.get(query_number).get(x)) && rel_doc_count==0 ){ //if doc is relevant 
                            rel_doc_count = rel_doc_count +1;
                            new_doc_rel.put(doc_id, query_indexed_docs.get(query_number).get(doc_id));
                            query_indexed_docs_rel.put(query_number,new_doc_rel);
                        }
                         
                        //turns data/cacm/CACM-1717.txt -> CACM-1717
                        String[] parts = doc_id.split("/");
                        String true_doc_id = parts[2];
                        String[] parts_2 = true_doc_id.split("\\.");                    
                        String true_doc_id2 = parts_2[0];
                         
                        //true if current doc (out of 7) is not part of relevant document array
                        boolean notMatch = !((query_indexed_rel_docs.get(query_number)).contains(true_doc_id2));
                         
                        if(notMatch && non_rel_doc_count==0){
                            non_rel_doc_count = non_rel_doc_count +1;
                            new_doc_nonrel.put(doc_id, query_indexed_docs.get(query_number).get(doc_id));
                            query_indexed_docs_nonrel.put(query_number,new_doc_nonrel);
                             
                        }
                         
                    }
                     
                     
                }
            }
             
            
            for(int i = 0; i < list_words.get(query_number).size(); i++){ //for each unique word
                  
                String current_unique_word= list_words.get(query_number).get(i);
                 
              //***Part A of Rocchio dealing with query
                if(query.get(query_number).containsKey(current_unique_word)){  //if word in query...works b/c key is term
                    double q_weight = query.get(query_number).get(current_unique_word);
                    rocchio_partA = A*q_weight;    
                }
                
                //***Part B of Rocchio dealing with rel docs
                if(query_indexed_docs_rel.size()==0){
                    rocchio_partB=0;
                }
                else{
                    for(String doc_id : query_indexed_docs_rel.get(query_number).keySet()){ //goes through top rated rel document   
                         
                        if(query_indexed_docs_rel.get(query_number).get(doc_id).containsKey(current_unique_word)){
                            double d_weight = query_indexed_docs_rel.get(query_number).get(doc_id).get(current_unique_word);
                            rocchio_partB = rocchio_partB + d_weight;                                
                        }
                                            
                    }
                }
                 
              //***Part C of Rocchio dealing with non-rel docs
                if(query_indexed_docs_nonrel.size()==0){
                    rocchio_partC=0;
                }
                else{
                    for(String doc_id : query_indexed_docs_nonrel.get(query_number).keySet()){ //goes through top rated rel document
                         
                        if(query_indexed_docs_nonrel.get(query_number).get(doc_id).containsKey(current_unique_word)){
                            double d_weight = query_indexed_docs_nonrel.get(query_number).get(doc_id).get(current_unique_word);
                            rocchio_partC = rocchio_partC + d_weight;   
                              
                        }
                                            
                    }
                }
                 
                 
                if(query_indexed_docs_rel.size()!=0){
                    rocchio_partB = B*(1.0/rel_doc_count)*rocchio_partB;
                }
                if(query_indexed_docs_nonrel.size()!=0){
                    rocchio_partC=C*(1.0/non_rel_doc_count)*rocchio_partC;
                }
               
                rocchio_score = rocchio_partA + rocchio_partB - rocchio_partC;
                 
                /* Compute Rocchio weights only for terms occurring in a relevant document or the query
                 *  (all other terms will have 0 or negative weights)
                 */
                if((rocchio_partA !=0) || (rocchio_partB !=0 )){ 
                    rocchio_term_Scores.put(current_unique_word, rocchio_score);
                }
                 
                rocchio_partA = 0;
                rocchio_partB = 0;
                rocchio_partC = 0;
                  
            }
             
             // sort
            LinkedHashMap<String, Double> sorted_terms = new LinkedHashMap<String, Double>();
            sorted_terms = sortHashMapByValuesD(rocchio_term_Scores);
 
            LinkedHashMap<String, Double> sorted_total = new LinkedHashMap<String, Double>();
            List<Entry<String, Double>> entryList = new ArrayList<Map.Entry<String, Double>>(
                    sorted_terms.entrySet());
       
            for (int y = entryList.size() - 1; y > 0; y--) {
                if (y >= 0) {
                    Entry<String, Double> lastEntry = entryList.get(y);
                    sorted_total.put(lastEntry.getKey(), lastEntry.getValue());
                }
              
            }
             
             int count_expand =0;
 
            for(String sorted_rocchio_term : sorted_total.keySet()){//go through sorted terms
 
                if(query.get(query_number).containsKey(sorted_rocchio_term)){
                     roc_value.put(sorted_rocchio_term, sorted_total.get(sorted_rocchio_term));
                }
                 
                if(count_expand < K){
                    int tf=0; 
                    if(!(query.get(query_number).containsKey(sorted_rocchio_term))){//if query does not contain term     
                        count_expand = count_expand + 1;
                         roc_value.put(sorted_rocchio_term, sorted_total.get(sorted_rocchio_term));
                    }
                }
                  
            }
            
            q_final.put(query_number, roc_value);
              
        }
 
        return q_final;
         
     
    }
      
    static HashMap<Integer, HashMap<String, Double>> query_weights
        (HashMap<Integer, HashMap<String, Integer[]>> query_index, 
            HashMap<String, HashMap<String, Integer[]>> doc_index){
        HashMap<Integer, HashMap<String, Double>> q_weights = new HashMap<Integer, HashMap<String, Double>>();
          
        for (Integer i: query_index.keySet()){
            HashMap<String, Double> weights = new HashMap<String, Double>();
            double normQuery = 0;
            double normConstQuery = 0;
            double normedQuery = 0;
              
            for (String k : query_index.get(i).keySet()) { // terms in query
                double tf = au_tf_query(query_index.get(i), k);
                 
                // System.out.println("doc size:"+ doc_index.size());
                // System.out.println("doc idf:"+
                // doc_index.get(j).get(k)[1]);
                  
                double idf = calc_idf(k, query_index.get(i).get(k)[1], doc_index.size());
                // double idf =calc_idf(k, doc_index.get(j).get(k)[0],
                // doc_index.size());
                double tf_idf = tf * idf;
                normQuery += tf_idf*tf_idf;
                // System.out.println(tf + " " +idf);
            }
            normConstQuery = Math.sqrt(normQuery);
              
            for (String m: query_index.get(i).keySet()){
                double tf_q = au_tf_query(query_index.get(i), m);
                double idf_q = calc_idf(m, query_index.get(i).get(m)[1], doc_index.size());
                normedQuery = (tf_q * idf_q) / normConstQuery;
                weights.put(m, normedQuery);
            }
            q_weights.put(i, weights);
              
        }
          
        return q_weights;
    }
      
    static HashMap<Integer, ArrayList<String>> unique_terms(HashMap<Integer, HashMap<String, HashMap<String, Double>>> rocchio_index, 
            HashMap<String, HashMap<String, Integer[]>> doc_index, HashMap<Integer, HashMap<String, Integer[]>> query_index){
          
        HashMap<Integer, ArrayList<String>> terms_per_query = new HashMap<Integer, ArrayList<String>>();
        //System.out.println(rocchio_index.size());
        for(Integer i: rocchio_index.keySet()){ //each query
            ArrayList<String> terms = new ArrayList<String>();
            for(String j: rocchio_index.get(i).keySet()){ //each of 7 documents in each query
                for (String k: doc_index.get(j).keySet()){
                    if (!terms.contains(k)){
                        terms.add(k);
                    }
                }
            }
              
            for(String m: query_index.get(i).keySet()){
                if (!terms.contains(m)){
                    terms.add(m);
                }
            }
              
            terms_per_query.put(i, terms);
        }
          
          
        return terms_per_query;
    }
      
    static HashMap<Integer, ArrayList<String>> processed_to_HashMap(String file) {
  
        HashMap<Integer, ArrayList<String>> in = new HashMap<Integer, ArrayList<String>>();
          
        try {
            //System.out.println("dsfdsf");
            BufferedReader br = new BufferedReader(new FileReader(file));
            //System.out.println("dsfdsf"); 
            String ln = br.readLine();
            while (ln != null) {
                String[] lnTerms = ln.split(" ");
               // System.out.println(lnTerms.toString());   
                ArrayList<String> indoc = new ArrayList<String>();
          
                for (int x = 1; x < lnTerms.length; x++) {
                    indoc.add(lnTerms[x]);
                }
          
               // System.out.println(lnTerms[0]);   
               // System.out.println(indoc);    
                in.put(Integer.parseInt(lnTerms[0]), indoc);
                ln = br.readLine();
      
            }
      
            br.close();
  
        } 
        catch (Exception e) {
        System.out.println("rel parsing error");
        }
        return in;
  
    }
      
      
    static HashMap<Integer, HashMap<String, HashMap<String, Double>>> read_rocchio_index
                (String file, String path, String sep){
          
        LinkedHashMap<Integer, HashMap<String, HashMap<String, Double>>> in = 
                new LinkedHashMap<Integer, HashMap<String, HashMap<String, Double>>>();
          
          
        try { // String path "data/cacm/CACM"
            BufferedReader br = new BufferedReader(new FileReader(file));
            String ln = br.readLine();
              
            while (ln != null) {
                //System.out.println(ln);
                if (ln.contains("queryxyz")){
                      
                    String[] q = ln.split("queryxyz");
                    Integer query = Integer.parseInt(q[0]);
                      
                    LinkedHashMap<String, HashMap<String, Double>> docs_terms = 
                            new LinkedHashMap<String, HashMap<String, Double>>();
                    while((ln = br.readLine()) != null && !ln.contains("queryxyz")){
                        if (ln.contains(path)) {
                            String[] lnTerms = br.readLine().split(";");
                            // System.out.println(Arrays.toString(lnTerms));
                            String[] lnWeight = br.readLine().split(";");
                            LinkedHashMap<String, Double> indoc = new LinkedHashMap<String, Double>();
            
                            for (int x = 0; x < lnTerms.length; x++) {
                                Double weight = Double.parseDouble(lnWeight[x]);
                                indoc.put(lnTerms[x], weight);
                            }
                            docs_terms.put(ln, indoc);
                        }
                    }
                      
                    in.put(query, docs_terms);
                }
            }
            br.close();
        } catch (Exception e) {
            System.out.println("csv parsing error");
        }
        return in;
          
    }
      
      
    static double atc_atc_new(HashMap<Integer, HashMap<String, Double>> query_index,
            HashMap<String, HashMap<String, Integer[]>> doc_index, String answerFile, String printFile) {
        double MAP = 0;
          
        //Date start = new Date();
       // Date end= new Date();
        //System.out.println(end.getTime() - start.getTime() + " total milliseconds");
          
          
      //Hashmap of query number, hashmap of doc name, hashmap of terms in doc -> value = cosine weight
        //HashMap<Integer, HashMap<String, HashMap<String, Double>>>;
        HashMap<Integer, HashMap<String, HashMap<String, Double>>> queries_docs_terms = 
                new HashMap<Integer, HashMap<String, HashMap<String, Double>>>();
          
        PrintWriter writer;
        try {
            writer = new PrintWriter("ROCCHIO_"+printFile, "UTF-8");
            
            for (Integer i : query_index.keySet()) { // queries
                //if(i==13){
                HashMap<String, Double> docScores = new HashMap<String, Double>();
                  
                HashMap<String, HashMap<String, Double>> docs_terms = new HashMap<String, HashMap<String, Double>>();
                  
                for (String j : doc_index.keySet()) { // docs
                    HashMap<String, Double> weights = atc_atc_new_weights(query_index, doc_index, i, j);
                     
                    double increment_weight = 0;
                    for (String weight: weights.keySet()){
                        increment_weight += weights.get(weight);
                    }
                    docScores.put(j, increment_weight);
                      
                    // System.out.println("Query:"+i+" "+docScores);
                }
                 
                 
                // sort
                ValueComparator bvc =  new ValueComparator(docScores);
                //http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
                TreeMap<String, Double> sorted_docs = new TreeMap<String, Double>(bvc);
  
                  
                sorted_docs.putAll(docScores); 
                //System.out.println(sorted_docs);
                // System.out.println("Query: "+ i);
                // System.out.println("Query: "+ i + sorted_docs);
        
                LinkedHashMap<String, Double> sorted_100 = new LinkedHashMap<String, Double>();
                LinkedHashMap<String, Double> sorted_7 = new LinkedHashMap<String, Double>();
                // sorted_100.put("", sorted_docs.get(y));
                // add some entries
                int count = 0;
                for (Map.Entry<String,Double> entry : sorted_docs.entrySet()) {
                    sorted_100.put(entry.getKey(), entry.getValue());
                    if (count < 7){
                        sorted_7.put(entry.getKey(), entry.getValue());
                    }
                    count++;
                    if (count > 99){
                        break;
                    }
                }
                    
                double AP =precision_LinkedMap(loadAnswers(answerFile).get(i), sorted_100);
                MAP += AP;
                //System.out.println("atc.atc Query: " + i + " AvgPrecision: " + AP + " " +  sorted_7);
                writer.println("atc.atc Query: " + i + " AvgPrecision: " + AP + " " +  sorted_7);
                  
            }//}
            MAP /= query_index.size();
            //writer.close();
           // writer = new PrintWriter("rocchio_index.txt", "UTF-8");
            //writer.println(queries_docs_terms);
            writer.close();
             
        } catch (FileNotFoundException e) { 
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
       // MAP /= query_index.size();
       // writer.println("atc.atc Collection MAP: " + MAP);
        //writer.close();
        //System.out.println(queries_docs_terms);
        //write_rocchio_index("rocchio_index.txt", queries_docs_terms);
          
        return MAP;
    }
      
      
    /*
     * QUESTION 3 PART A atc.atc weighting
     */
    //writes the rocchio index
    //calculates original atc_atc score
    static double atc_atc(HashMap<Integer, HashMap<String, Integer[]>> query_index,
            HashMap<String, HashMap<String, Integer[]>> doc_index, String answerFile, String printFile, String rocFile) {
        double MAP = 0;
          
        //Date start = new Date();
       // Date end= new Date();
        //System.out.println(end.getTime() - start.getTime() + " total milliseconds");
          
          
      //Hashmap of query number, hashmap of doc name, hashmap of terms in doc -> value = cosine weight
        //HashMap<Integer, HashMap<String, HashMap<String, Double>>>;
        HashMap<Integer, HashMap<String, HashMap<String, Double>>> queries_docs_terms = 
                new HashMap<Integer, HashMap<String, HashMap<String, Double>>>();
          
        PrintWriter writer;
        try {
            writer = new PrintWriter(printFile, "UTF-8");
            
            for (Integer i : query_index.keySet()) { // queries
                //if(i==13){
                HashMap<String, Double> docScores = new HashMap<String, Double>();
                  
                LinkedHashMap<String, HashMap<String, Double>> docs_terms = new  LinkedHashMap<String, HashMap<String, Double>>();
                  
                  
                for (String j : doc_index.keySet()) { // docs
                    HashMap<String, Double> weights = atc_atc_weights(query_index, doc_index, i, j);
                    double increment_weight = 0;
                    for (String weight: weights.keySet()){
                        increment_weight += weights.get(weight);
                    }
                    docScores.put(j, increment_weight);
                      
                    // System.out.println("Query:"+i+" "+docScores);
                }
                //if(i==13){
                    //System.out.println("Query:"+i+" "+docScores);
                //}
                // sort
                ValueComparator bvc =  new ValueComparator(docScores);
                //http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
                TreeMap<String, Double> sorted_docs = new TreeMap<String, Double>(bvc);
  
                  
                sorted_docs.putAll(docScores); 
                //System.out.println(sorted_docs);
                // System.out.println("Query: "+ i);
                // System.out.println("Query: "+ i + sorted_docs);
        
                LinkedHashMap<String, Double> sorted_100 = new LinkedHashMap<String, Double>();
                LinkedHashMap<String, Double> sorted_7 = new LinkedHashMap<String, Double>();
                // sorted_100.put("", sorted_docs.get(y));
                // add some entries
                int count = 0;
                for (Map.Entry<String,Double> entry : sorted_docs.entrySet()) {
                    sorted_100.put(entry.getKey(), entry.getValue());
                    if (count < 7){
                        sorted_7.put(entry.getKey(), entry.getValue());
                    }
                    count++;
                    if (count > 99){
                        break;
                    }
                }
                    
                double AP =precision_LinkedMap(loadAnswers(answerFile).get(i), sorted_100);
                MAP += AP;
                //System.out.println("atc.atc Query: " + i + " AvgPrecision: " + AP + " " +  sorted_7);
                writer.println("atc.atc Query: " + i + " AvgPrecision: " + AP + " " +  sorted_7);
                  
                for (String rel_doc: sorted_7.keySet()){
                     
                    docs_terms.put(rel_doc, atc_atc_doc_weights(query_index, doc_index, i, rel_doc));
                }
                queries_docs_terms.put(i, docs_terms);
                  
            }//}
            MAP /= query_index.size();
              
            //writer = new PrintWriter("rocchio_index.txt", "UTF-8");
            //writer.println(queries_docs_terms);
            writer.close();
             
        } catch (Exception e) { 
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
       // MAP /= query_index.size();
       // writer.println("atc.atc Collection MAP: " + MAP);
        //writer.close();
       // System.out.println("Inside atc_atc queries_docs_terms"+queries_docs_terms);
        //rocchio_index_med.txt
        write_rocchio_index(rocFile, queries_docs_terms);
          
        return MAP;
    }
    
    //returns rocchio weights
    static HashMap<String, Double> atc_atc_new_weights( HashMap<Integer, HashMap<String, Double>> query_index,
            HashMap<String, HashMap<String, Integer[]>> doc_index, Integer i, String j){
        double normQuery = 0;
        double normDoc = 0;
        double normConstQuery = 0;
        double normConstDoc = 0;
        double normedDoc = 0;
        double normedQuery = 0;
        //double increment_weight = 0;
          
        HashMap<String, Double> weights = new HashMap<String, Double>();
          
        HashMap<String, Integer[]> doc_map =  doc_index.get(j);
        int max_tf = 0;
        for (String key : doc_map.keySet()) {
            if (doc_map.get(key)[0] > max_tf) {
                max_tf = doc_map.get(key)[0];
            }
        }
          
        for (String k : doc_index.get(j).keySet()) { // terms in docs
            // System.out.println("String " + k);           
              
            double tf = au_tf(doc_map.get(k)[0], max_tf);
            // System.out.println("TF of doc" + tf);
            double idf = calc_idf(k, doc_index.get(j).get(k)[1],
                    doc_index.size());
            // System.out.println("IDF of doc" + idf);
            double tf_idf = tf * idf;
            normDoc += tf_idf*tf_idf;
        }
        normConstDoc = Math.sqrt(normDoc);
          
          
        HashMap<String, Double> query_map =  query_index.get(i);
        max_tf = 0;
         
        /*
        for (String key : query_map.keySet()) {
            if (query_map.get(key)[0] > max_tf) {
                max_tf = query_map.get(key)[0];
            }
        } */
          
        for (String k : query_index.get(i).keySet()) { // terms in query
            //double tf = au_tf(query_map.get(k)[0], max_tf);
            // System.out.println("k:"+ k);
            // System.out.println("doc size:"+ doc_index.size());
            // System.out.println("doc idf:"+
            // doc_index.get(j).get(k)[1]);
              
            //double idf = calc_idf(k, query_index.get(i).get(k)[1], doc_index.size());
            // double idf =calc_idf(k, doc_index.get(j).get(k)[0],
            // doc_index.size());
            double roc_weight = query_map.get(k);
            //double tf_idf = tf * idf;
            normQuery += roc_weight*roc_weight;
            // System.out.println(tf + " " +idf);
        }
        normConstQuery = Math.sqrt(normQuery);
          
          
        for (String m : query_index.get(i).keySet()) { // terms in
                                                        // queries
            for (String n : doc_index.get(j).keySet()) { // terms in doc
                if (m.equals(n)) { // terms that are same in both doc
                                    // and query
  
                    // normalized tf_idf for term in doc
                    double tf = au_tf(doc_index.get(j), n);
                    double idf = calc_idf(n,
                            doc_index.get(j).get(n)[1],
                            doc_index.size());
                    normedDoc = (tf * idf) / normConstDoc;
  
                    // normalized tf_idf for term in query
                    /*double tf_q = au_tf_query(query_index.get(i), m);
                    double idf_q = calc_idf(m,
                            doc_index.get(j).get(m)[1],
                            doc_index.size());
                    normedQuery = (tf_q * idf_q) / normConstQuery;*/
                    //increment_weight += (normedDoc * normedQuery);
                    normedQuery =  query_map.get(m)/normConstQuery;
                    weights.put(n, normedDoc*normedQuery);
                }
            }
        }
          
        return weights;
    }
      
    //returns only normedDoc weight (returns vector of term and its weights)
    static HashMap<String, Double> atc_atc_doc_weights( HashMap<Integer, HashMap<String, Integer[]>> query_index,
            HashMap<String, HashMap<String, Integer[]>> doc_index, Integer i, String j){
        double normQuery = 0;
        double normDoc = 0;
        double normConstQuery = 0;
        double normConstDoc = 0;
        double normedDoc = 0;
        double normedQuery = 0;
        //double increment_weight = 0;
          
        HashMap<String, Double> weights = new HashMap<String, Double>();
          
        HashMap<String, Integer[]> doc_map =  doc_index.get(j);
        int max_tf = 0;
        for (String key : doc_map.keySet()) {
            if (doc_map.get(key)[0] > max_tf) {
                max_tf = doc_map.get(key)[0];
            }
        }
          
        for (String k : doc_index.get(j).keySet()) { // terms in docs
            // System.out.println("String " + k);           
              
            double tf = au_tf(doc_map.get(k)[0], max_tf);
            // System.out.println("TF of doc" + tf);
            double idf = calc_idf(k, doc_index.get(j).get(k)[1],
                    doc_index.size());
            // System.out.println("IDF of doc" + idf);
            double tf_idf = tf * idf;
            normDoc += tf_idf*tf_idf;
        }
        normConstDoc = Math.sqrt(normDoc);
          
          
        HashMap<String, Integer[]> query_map =  query_index.get(i);
        max_tf = 0;
        for (String key : query_map.keySet()) {
            if (query_map.get(key)[0] > max_tf) {
                max_tf = query_map.get(key)[0];
            }
        }
          
        for (String k : query_index.get(i).keySet()) { // terms in query
            double tf = au_tf(query_map.get(k)[0], max_tf);
            // System.out.println("k:"+ k);
            // System.out.println("doc size:"+ doc_index.size());
            // System.out.println("doc idf:"+
            // doc_index.get(j).get(k)[1]);
              
            double idf = calc_idf(k, query_index.get(i).get(k)[1], doc_index.size());
            // double idf =calc_idf(k, doc_index.get(j).get(k)[0],
            // doc_index.size());
            double tf_idf = tf * idf;
            normQuery += tf_idf*tf_idf;
            // System.out.println(tf + " " +idf);
        }
        normConstQuery = Math.sqrt(normQuery);
          
          
        //for (String m : query_index.get(i).keySet()) { // terms in
                                                        // queries
            for (String n : doc_index.get(j).keySet()) { // terms in doc
                 // normalized tf_idf for term in doc
                double tf = au_tf(doc_index.get(j), n);
                double idf = calc_idf(n,
                        doc_index.get(j).get(n)[1],
                        doc_index.size());
                normedDoc = (tf * idf) / normConstDoc;
 
                // normalized tf_idf for term in query
                /*double tf_q = au_tf_query(query_index.get(i), m);
                double idf_q = calc_idf(m,
                        doc_index.get(j).get(m)[1],
                        doc_index.size());
                normedQuery = (tf_q * idf_q) / normConstQuery;*/
                //increment_weight += (normedDoc * normedQuery);
                weights.put(n, normedDoc);
            }
        //}
          
        return weights;
    }
    //Returns the atc_atc weights for each individual term in the document with respect to query
    //int i -> query number, String j -> doc name
    //returns weights for normedDoc*normedQuery
    static HashMap<String, Double> atc_atc_weights(HashMap<Integer, HashMap<String, Integer[]>> query_index,
            HashMap<String, HashMap<String, Integer[]>> doc_index, Integer i, String j){
        double normQuery = 0;
        double normDoc = 0;
        double normConstQuery = 0;
        double normConstDoc = 0;
        double normedDoc = 0;
        double normedQuery = 0;
        //double increment_weight = 0;
          
        HashMap<String, Double> weights = new HashMap<String, Double>();
          
        HashMap<String, Integer[]> doc_map =  doc_index.get(j);
        int max_tf = 0;
        for (String key : doc_map.keySet()) {
            if (doc_map.get(key)[0] > max_tf) {
                max_tf = doc_map.get(key)[0];
            }
        }
          
        for (String k : doc_index.get(j).keySet()) { // terms in docs
            // System.out.println("String " + k);           
              
            double tf = au_tf(doc_map.get(k)[0], max_tf);
            // System.out.println("TF of doc" + tf);
            double idf = calc_idf(k, doc_index.get(j).get(k)[1],
                    doc_index.size());
            // System.out.println("IDF of doc" + idf);
            double tf_idf = tf * idf;
            normDoc += tf_idf*tf_idf;
        }
        normConstDoc = Math.sqrt(normDoc);
          
          
        HashMap<String, Integer[]> query_map =  query_index.get(i);
        max_tf = 0;
        for (String key : query_map.keySet()) {
            if (query_map.get(key)[0] > max_tf) {
                max_tf = query_map.get(key)[0];
            }
        }
          
        for (String k : query_index.get(i).keySet()) { // terms in query
            double tf = au_tf(query_map.get(k)[0], max_tf);
            // System.out.println("k:"+ k);
            // System.out.println("doc size:"+ doc_index.size());
            // System.out.println("doc idf:"+
            // doc_index.get(j).get(k)[1]);
              
            double idf = calc_idf(k, query_index.get(i).get(k)[1], doc_index.size());
            // double idf =calc_idf(k, doc_index.get(j).get(k)[0],
            // doc_index.size());
            double tf_idf = tf * idf;
            normQuery += tf_idf*tf_idf;
            // System.out.println(tf + " " +idf);
        }
        normConstQuery = Math.sqrt(normQuery);
          
          
        for (String m : query_index.get(i).keySet()) { // terms in
                                                        // queries
            for (String n : doc_index.get(j).keySet()) { // terms in doc
                if (m.equals(n)) { // terms that are same in both doc
                                    // and query
  
                    // normalized tf_idf for term in doc
                    double tf = au_tf(doc_index.get(j), n);
                    double idf = calc_idf(n,
                            doc_index.get(j).get(n)[1],
                            doc_index.size());
                    normedDoc = (tf * idf) / normConstDoc;
  
                    // normalized tf_idf for term in query
                    double tf_q = au_tf_query(query_index.get(i), m);
                    double idf_q = calc_idf(m,
                            doc_index.get(j).get(m)[1],
                            doc_index.size());
                    normedQuery = (tf_q * idf_q) / normConstQuery;
                    //increment_weight += (normedDoc * normedQuery);
                    weights.put(n, normedDoc*normedQuery);
                }
            }
        }
          
        return weights;
    }
      
    static HashMap<String, Double> atc_atc_weights_dense(HashMap<Integer, HashMap<String, Integer[]>> query_index,
            HashMap<String, HashMap<String, Integer[]>> doc_index, Integer i, String j){
        double normQuery = 0;
        double normDoc = 0;
        //double increment_weight = 0;
          
        HashMap<String, Double> weights = new HashMap<String, Double>();
          
        HashMap<String, Integer[]> doc_map =  doc_index.get(j);
        int max_tf = 0;
        for (String key : doc_map.keySet()) {
            if (doc_map.get(key)[0] > max_tf) {
                max_tf = doc_map.get(key)[0];
            }
        }
          
        HashMap<String, Integer[]> query_map =  query_index.get(i);
        int max_tf_q = 0;
        for (String key : query_map.keySet()) {
            if (query_map.get(key)[0] > max_tf_q) {
                max_tf_q = query_map.get(key)[0];
            }
        }
  
        //for (String k : doc_index.get(j).keySet()) {
            //for (String k : query_index.get(i).keySet()) {
          
        for (String m : query_index.get(i).keySet()) { // terms in
                                                        // queries
            double tf_q = au_tf(query_map.get(m)[0], max_tf_q);
            double idf_q = calc_idf(m, query_index.get(i).get(m)[1], doc_index.size());
            double tf_idf_q = tf_q * idf_q;
              
            for (String n : doc_index.get(j).keySet()) { // terms in doc
  
                // normalized tf_idf for term in doc
                double tf = au_tf(doc_map.get(n)[0], max_tf);
                double idf = calc_idf(n, doc_index.get(j).get(n)[1], doc_index.size());
                double tf_idf = tf * idf;
                //normedDoc = (tf * idf) / normConstDoc;
                // normalized tf_idf for term in query
                //normedQuery = (tf_q * idf_q) / normConstQuery;
                  
                if (m.equals(n)) { // terms that are same in both doc and query
                    //increment_weight += (normedDoc * normedQuery);
                    weights.put(n, tf_idf * tf_idf_q);
                }
                normDoc += tf_idf * tf_idf;
            }
            normQuery += tf_idf_q * tf_idf_q;
        }
          
        for (String n: weights.keySet()){
            weights.put(n, weights.get(n)/Math.sqrt(normDoc*normQuery));
        }
          
        return weights;
    }
      
     
        
    // Parameters: file name, file folder path, separator used in CSV file
    //Converts csv file data to usable data format
    static HashMap<String, HashMap<String, Integer[]>> csv_to_HashMap(
            String file, String path, String sep) {
        HashMap<String, HashMap<String, Integer[]>> in = new HashMap<String, HashMap<String, Integer[]>>();
    
        try { // String path "data/cacm/CACM"
            BufferedReader br = new BufferedReader(new FileReader(file));
            String ln = br.readLine();
    
            while (ln != null) {
                if (ln.contains(path)) {
                    String[] lnTerms = br.readLine().split(";");
                    // System.out.println(Arrays.toString(lnTerms));
                    String[] lnTF = br.readLine().split(";");
                    String[] lnDF = br.readLine().split(";");
    
                    HashMap<String, Integer[]> indoc = new HashMap<String, Integer[]>();
    
                    for (int x = 0; x < lnTerms.length; x++) {
                        Integer[] arr = new Integer[2];
                        arr[0] = Integer.parseInt(lnTF[x]);
                        arr[1] = Integer.parseInt(lnDF[x]);
                        indoc.put(lnTerms[x], arr);
                    }
                    in.put(ln, indoc);
                }
                ln = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            System.out.println("csv parsing error");
        }
        return in;
    }
        
    //builds and returns the all the queries in usable format, tokenized, stemmed, stopworded, indexed
    private static HashMap<Integer, HashMap<String, Integer>> buildQueryMap(
            Map<Integer, String> queries, Analyzer analyzer) {
        HashMap<Integer, HashMap<String, Integer>> allQueries = new HashMap<Integer, HashMap<String, Integer>>();
        try {
            for (Integer i : queries.keySet()) {
                HashMap<String, Integer> queryIndex = new HashMap<String, Integer>();
                TokenStream ts = analyzer.tokenStream(null, queries.get(i));
                ts.reset();
                PorterStemmer stemmer = new PorterStemmer();
                while (ts.incrementToken()) {
                    // System.out.println(ts.getAttribute(CharTermAttribute.class).toString());
                    stemmer.setCurrent(ts.getAttribute(CharTermAttribute.class)
                            .toString());
                    stemmer.stem();
                    String stem = stemmer.getCurrent();
                    // System.out.println("stem: " + stem);
                    if (queryIndex.containsKey(stem)) {
                        queryIndex.put(stem, queryIndex.get(stem) + 1);
                    } else {
                        queryIndex.put(stem, 1);
                    }
                }
                ts.end();
                ts.close();
                allQueries.put(i, queryIndex);
            }
        } catch (Exception e) {
            System.out.println(" caught a " + e.getClass()
                    + "\n with message: " + e.getMessage());
        }
        return allQueries;
    
    }
      
  //builds and returns the all the queries in usable format, tokenized, stemmed, stopworded, indexed, with IDF
    private static HashMap<Integer, HashMap<String, Integer[]>> buildQueryMap_idf(
            Map<Integer, String> queries, Analyzer analyzer, HashMap<String, HashMap<String, Integer []>> doc_index) {
        HashMap<Integer, HashMap<String, Integer[]>> allQueries = new HashMap<Integer, HashMap<String, Integer[]>>();
        try {
            for (Integer i : queries.keySet()) {
                HashMap<String, Integer[]> queryIndex = new HashMap<String, Integer[]>();
                TokenStream ts = analyzer.tokenStream(null, queries.get(i));
                ts.reset();
                PorterStemmer stemmer = new PorterStemmer();
                while (ts.incrementToken()) {
                    // System.out.println(ts.getAttribute(CharTermAttribute.class).toString());
                    stemmer.setCurrent(ts.getAttribute(CharTermAttribute.class)
                            .toString());
                    stemmer.stem();
                    String stem = stemmer.getCurrent();
                    // System.out.println("stem: " + stem);
                    if (queryIndex.containsKey(stem)) {
                        Integer[] x = new Integer[2];
                        x[0] = queryIndex.get(stem)[0] + 1;
                        x[1] = queryIndex.get(stem)[1];
                        queryIndex.put(stem, x);
                    } else {
                        Integer[] x = new Integer[2];
                        x[0] = 1;
                        x[1] = 0;
                        for (String key : doc_index.keySet()) {
                            HashMap<String, Integer[]> indexDoc = doc_index.get(key);
                              
                            if(indexDoc.containsKey(stem)){
                                x[1] = indexDoc.get(stem)[1];
                                break;
                            }
                              
                        }
                          
                        queryIndex.put(stem, x);
                    }
                }
                ts.end();
                ts.close();
                allQueries.put(i, queryIndex);
            }
        } catch (Exception e) {
            System.out.println(" caught a " + e.getClass()
                    + "\n with message: " + e.getMessage());
        }
        return allQueries;
    
    }
      
    
    public static LinkedHashMap<String, Double> sortHashMapByValuesD(HashMap<String, Double> passedMap) {
        List<String> mapKeys = new ArrayList<String>(passedMap.keySet());
        List<Double> mapValues = new ArrayList<Double>(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);
    
        LinkedHashMap<String, Double> sortedMap = new LinkedHashMap<String, Double>();
    
        Iterator<Double> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Object val = valueIt.next();
            Iterator<String> keyIt = mapKeys.iterator();
    
            while (keyIt.hasNext()) {
                Object key = keyIt.next();
                String comp1 = passedMap.get(key).toString();
                String comp2 = val.toString();
    
                if (comp1.equals(comp2)) {
                    passedMap.remove(key);
                    mapKeys.remove(key);
                    sortedMap.put((String) key, (Double) val);
                    break;
                }
            }
        }
        return sortedMap;
    }
    
    //Calculates idf measure with log if df is already known
    private static double calc_idf(String term, Integer df, Integer N) {
        if (df == 0){
            return 0;
        }
        return Math.log((double) N / (double) df);
    }
    
     
    
    //augmented term frequency
    private static double au_tf_query(HashMap<String, Integer[]> map, String term) {
        int tf = map.get(term)[0];
        int max_tf = 0;
        for (String key : map.keySet()) {
            if (map.get(key)[0] > max_tf) {
                max_tf = map.get(key)[0];
            }
        }
    
        return 0.5 + ((0.5 * tf) / max_tf);
    
    }
      
  //augmented term frequency, same as au_tf method except with df-inclusive hashmap
    private static double au_tf(HashMap<String, Integer[]> map, String term) {
        int tf = map.get(term)[0];
        int max_tf = 0;
        for (String key : map.keySet()) {
            if (map.get(key)[0] > max_tf) {
                max_tf = map.get(key)[0];
            }
        }
    
        return 0.5 + ((0.5 * tf) / max_tf);
    
    }
      
    //known max_tf
    private static double au_tf(Integer tf, Integer max_tf) {  
        return 0.5 + ((0.5 * tf) / max_tf);
    
    }
  
    private static void write_rocchio_index(String fileName,
            HashMap<Integer, HashMap<String, HashMap<String, Double>>> index) {
          
        try {
            PrintWriter writer;
            writer = new PrintWriter(fileName, "UTF-8");
            for(Integer qry: index.keySet()){
                writer.println(qry+"queryxyz");
                for (String doc : index.get(qry).keySet()) {
                    writer.println(doc);
                    String stems = "";
                    String counts = "";
        
                    HashMap<String, Double> indexDoc = index.get(qry).get(doc);
                    for (String s : indexDoc.keySet()) {
                        // if (s.contains(",")){ //prevent commas from screwing up
                        // csv file
                        // s = s.replace(",", "");
                        // }
                        stems += s + ";";
                        counts += indexDoc.get(s) + ";";
                    }
        
                    writer.println(stems);
                    writer.println(counts);
                }
            }
              
    
            writer.close();
    
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
          
          
    }
      
      
    //writes index to a file in csv format, with document name on 1st line, 
    //terms (stems) on 2nd line, and counts of each term, respectively, on 3rd line
    private static void writeIndex(String fileName,
            HashMap<String, HashMap<String, Integer>> index) {
        try {
            PrintWriter writer;
            writer = new PrintWriter(fileName, "UTF-8");
    
            for (String key : index.keySet()) {
                writer.println(key);
                String stems = "";
                String counts = "";
    
                HashMap<String, Integer> indexDoc = index.get(key);
                for (String s : indexDoc.keySet()) {
                    // if (s.contains(",")){ //prevent commas from screwing up
                    // csv file
                    // s = s.replace(",", "");
                    // }
                    stems += s + ";";
                    counts += indexDoc.get(s) + ";";
                }
    
                writer.println(stems);
                writer.println(counts);
            }
    
            writer.close();
    
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    
    }
      
      
  
    //writes index containing document frequency information to a file in csv format, 
    //with document name on 1st line, terms (stems) on 2nd line 
    //counts of each term, respectively, on 3rd line, and
    //document frequencies for each term, respectively, on 4th line
    private static void writeIdfIndex(String fileName,
            HashMap<String, HashMap<String, Integer[]>> index) {
        try {
            PrintWriter writer;
            writer = new PrintWriter(fileName, "UTF-8");
    
            for (String key : index.keySet()) {
                writer.println(key);
                String stems = "";
                String counts = "";
                String IDFcounts = "";
    
                HashMap<String, Integer[]> indexDoc = index.get(key);
                for (String s : indexDoc.keySet()) {
                    // if (s.contains(",")){ //prevent commas from screwing up
                    // csv file
                    // s = s.replace(",", "");
                    // }
                    stems += s + ";";
                    counts += indexDoc.get(s)[0] + ";";
                    IDFcounts += indexDoc.get(s)[1] + ";";
                }
    
                writer.println(stems);
                writer.println(counts);
                writer.println(IDFcounts);
            }
    
            writer.close();
    
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    
    }
    private static void writeIdfIndex_query(String fileName,
            HashMap<Integer, HashMap<String, Integer[]>> index) {
        try {
            PrintWriter writer;
            writer = new PrintWriter(fileName, "UTF-8");
    
            for (Integer key : index.keySet()) {
                writer.println(key);
                String stems = "";
                String roc_weight = "";
               String IDFcounts = "";
    
                HashMap<String, Integer[]> indexDoc = index.get(key);
                for (String s : indexDoc.keySet()) {
                    // if (s.contains(",")){ //prevent commas from screwing up
                    // csv file
                    // s = s.replace(",", "");
                    // }
                    stems += s + ";";
                    roc_weight += indexDoc.get(s)[0] + ";";
                    IDFcounts += indexDoc.get(s)[1] + ";";
                }
    
                writer.println(stems);
                writer.println(roc_weight);
                writer.println(IDFcounts);
            }
    
            writer.close();
    
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    
    }
    private static void writeIndex_query(String fileName,
            HashMap<Integer, HashMap<String, Double>> index) {
        try {
            PrintWriter writer;
            writer = new PrintWriter(fileName, "UTF-8");
    
            for (Integer key : index.keySet()) {
                writer.println(key);
                String stems = "";
                String roc_weight = "";
               // String IDFcounts = "";
    
                HashMap<String, Double> indexDoc = index.get(key);
                for (String s : indexDoc.keySet()) {
                    // if (s.contains(",")){ //prevent commas from screwing up
                    // csv file
                    // s = s.replace(",", "");
                    // }
                    stems += s + ";";
                    roc_weight += indexDoc.get(s) + ";";
                    //IDFcounts += indexDoc.get(s)[1] + ";";
                }
    
                writer.println(stems);
                writer.println(roc_weight);
               // writer.println(IDFcounts);
            }
    
            writer.close();
    
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    
    }
      
    //unchanged
    private static Map<Integer, String> loadQueries(String filename) {
        HashMap<Integer, String> queryIdMap = new HashMap<Integer, String>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(new File(filename)));
        } catch (FileNotFoundException e) {
            System.out.println(" caught a " + e.getClass()
                    + "\n with message: " + e.getMessage());
        }
    
        String line;
        try {
            while ((line = in.readLine()) != null) {
                int pos = line.indexOf(',');
                queryIdMap.put(Integer.parseInt(line.substring(0, pos)),
                        line.substring(pos + 1));
            }
        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass()
                    + "\n with message: " + e.getMessage());
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                System.out.println(" caught a " + e.getClass()
                        + "\n with message: " + e.getMessage());
            }
        }
        return queryIdMap;
    }
  
    //unchanged
    private static Map<Integer, HashSet<String>> loadAnswers(String filename) {
        HashMap<Integer, HashSet<String>> queryAnswerMap = new HashMap<Integer, HashSet<String>>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(new File(filename)));
    
            String line;
            while ((line = in.readLine()) != null) {
                String[] parts = line.split(" ");
                HashSet<String> answers = new HashSet<String>();
                for (int i = 1; i < parts.length; i++) {
                    answers.add(parts[i]);
                }
                queryAnswerMap.put(Integer.parseInt(parts[0]), answers);
            }
        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass()
                    + "\n with message: " + e.getMessage());
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                System.out.println(" caught a " + e.getClass()
                        + "\n with message: " + e.getMessage());
            }
        }
        return queryAnswerMap;
    }
    
    // Average precision
    private static double precision(HashSet<String> answers,
            List<String> results) {
        double matches = 0;
        double index = 0;
        double sum = 0;
        for (String result : results) {
            index++;
            if (answers.contains(result)) {
                matches++;
                // System.out.println("result: "+result);
                // System.out.println("matches: "+matches);
                // System.out.println("precision" + matches / index);
                sum += matches / index;
            }
        }
    
        return sum / answers.size();
    }
      
    //Same as precision method, except takes in a linked hashmap
    private static double precision_LinkedMap(HashSet<String> answers,
            LinkedHashMap<String, Double> results) {
        int matches = 0;
        int index = 0;
        double sum = 0;
        HashSet<String> matchedAnswers = new HashSet<String>();
        for (String result : results.keySet()) {
            index++;
            for (String answer: answers){
                //System.out.println(answer + " " + index);
                if (result.contains(answer)) {
                    matches++;
                    // System.out.println("result: "+result);
                    // System.out.println("matches: "+matches);
                    // System.out.println("precision" + matches / index);
                    sum += (double)matches / index;
                    matchedAnswers.add(answer);
                    //break;
                }
            }
        }
            
            
        return sum / answers.size();
    }
      
    //Same as original
    private static double evaluate(String indexDir, String docsDir,
            String queryFile, String answerFile, int numResults,
            CharArraySet stopwords) {
    
        // Build Index
        IndexFiles.buildIndex_1(indexDir, docsDir, stopwords);
    
        // load queries and answer
        Map<Integer, String> queries = loadQueries(queryFile);
        Map<Integer, HashSet<String>> queryAnswers = loadAnswers(answerFile);
    
        // Search and evaluate
        double sum = 0;
        for (Integer i : queries.keySet()) {
            List<String> results = SearchFiles.searchQuery(indexDir,
                    queries.get(i), numResults, stopwords);
            double prec = precision(queryAnswers.get(i), results); 
            sum += prec;
            //System.out.println(prec);
            // System.out.printf("\nTopic %d  ", i);
            // System.out.print (results);
            // System.out.println();
    
        }
    
        return sum / queries.size();
    }
      
    //Creates a map listing of query strings, parsing and stopwording each query
    //identified by their query number as the key
    private static Map<Integer, String> stopwordQueryMap(Analyzer analyzer, String queryFile){
        Map<Integer, String> queries = loadQueries(queryFile);
        try {
            QueryParser parser = new QueryParser("", analyzer);
    
            for (Integer i : queries.keySet()) {
                Query query;
                query = parser.parse(QueryParser.escape(queries.get(i)));
                queries.put(i, query.toString());
            }
        } catch (Exception e) {
            System.out.println(" caught a " + e.getClass()
                    + "\n with message: " + e.getMessage());
        }
        return queries;
    }
      
}