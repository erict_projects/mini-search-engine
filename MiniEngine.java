import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;


public class MiniEngine {

	public MiniEngine() {
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Indexes the given file using the given writer, or if a directory is given,
	 * recurses over files and directories found under the given directory.
	 */
	static void indexDocs(IndexWriter writer, File file, CharArraySet stopwords) {
		if (file.canRead()) {
			if (file.isDirectory()) {
				String[] files = file.list();
				if (files != null) {
					for (int i = 0; i < files.length; i++) {
						indexDocs(writer, new File(file, files[i]), null);
					}
				}
			} else {
				FileInputStream fis = null;
				
				try {
					fis = new FileInputStream(file);
				} catch (FileNotFoundException e) {
					System.out.println(" caught a " + e.getClass() + "\n with message: " + e.getMessage());
				}

				try {
					// make a new, empty document
					Doc doc = new Doc();

					// Add the path of the file as a field named "path".  Use a
					// field that is indexed (i.e. searchable), but don't tokenize 
					// the field into separate words and don't index term frequency
					// or positional information:
					String pathField = file.getName();
					doc.setPath(pathField);
					
					Analyzer analyzer = new MyAnalyzer(stopwords);
					
					TokenStream tokenStream = analyzer.tokenStream(null, new BufferedReader(new InputStreamReader(fis)));
					OffsetAttribute offsetAttribute = tokenStream.getAttribute(OffsetAttribute.class);
					CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);

					tokenStream.reset();
					while (tokenStream.incrementToken()) {
					    int startOffset = offsetAttribute.startOffset();
					    int endOffset = offsetAttribute.endOffset();
					    String term = charTermAttribute.toString();
					}
					/*
					  CharTermAttribute cattr = stream.addAttribute(CharTermAttribute.class);
						stream.reset();
						while (stream.incrementToken()) {
						  System.out.println(cattr.toString());
						}
						stream.end();
						stream.close();
					 */
					// Add the contents of the file to a field named "contents".  Specify a Reader,
					// so that the text of the file is tokenized and indexed, but not stored.
					//doc.add(new TextField("contents", new BufferedReader(new InputStreamReader(fis))));

					// New index, so we just add the document (no old document can be there):
					// System.out.println("adding " + file);
					//writer.addDocument(doc);

				} catch (Exception e) {
					System.out.println(" caught a " + e.getClass() + "\n with message: " + e.getMessage());
				} finally {
					try {
						fis.close();
					} catch(IOException e) {
						System.out.println(" caught a " + e.getClass() + "\n with message: " + e.getMessage());
					}
				}
			}
		}
	}

}
