Source code from a Search Engine project in Information Retrieval course (CS 4300).
Many of the lines in the code are for various query/document weight algorithms, to be run on two collections of thousands of documents, namely "CACM" and "MED".
The goal was to read a number of queries, and compare each query to each document in the collection, resulting in a relevance score for each document to each query. Then, we sort them and compare our results to a list of human-determined "answers" for a precision score for each algorithm. 
Collections were parsed using a Lucene tokenizer and stemmer, and data stored in CSV files to be read into memory for use by the algorithms (such as atc.atc weighting, etc...) 

*I know there are several parts of the code that can be cleaned up or made more efficient, but time was a limiting factor.